import unittest
from my_factorial import my_factorial

class TestCase(unittest.TestCase):
    def test_normal(self):
        self.assertEqual(my_factorial(5), 120)

    def test_zero(self):
        self.assertEqual(my_factorial(0), 1)
