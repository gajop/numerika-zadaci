class MyFactorial {
    public int calculate(int x) {
        int y = 1;
        for (int i = 1; i <= y; i++) {
            y *= i;
        }
        return y;
    }
}
