class Test {
    @Test 
    public void testDefault() {
        MyFactorial myFactorial = new MyFactorial();
        assertEquals("MyFactorial.calculate(5) must equal 120", 120, myFactorial.calculate(5));
    }

    @Test 
    public void testZero() {
        MyFactorial myFactorial = new MyFactorial();
        assertEquals("MyFactorial.calculate(0) must equal 1", 1, myFactorial.calculate(0));
    }
}
