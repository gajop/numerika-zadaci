function x = secica(f, x1, x2, itMax, errMax)
for i = 1:itMax    
    y1 = feval(f, x1);
    y2 = feval(f, x2);
    a=(y1-y2)/(x1-x2);
    b=y1-a*x1;
    x = -b/a;
    y = feval(f, x);
    
    if(abs(y)<errMax)
	return;
    end
    if(y1*y<0)
	x2 = x;
    else
	x1 = x;
    end
end
        