Poluprečnik valjka
Izračunati poluprečnik osnove valjka r, visine h=5m, tako da zapremina bude V=20m3. Koristiti metodu sečice.
Metodu sečice implementirati u funkciji secica. Funkciju koja se koristi pri pozivu za metodu secice implementirati u funkciji zadatak2.
U funkciji zapremina izračunati poluprečnik.