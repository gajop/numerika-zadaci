function fs = tests()
    fs = { 
        struct('f',@test1, 'name','test1', 'err','Konačan rezultat nije tačan')
        struct('f',@test2, 'name','test2', 'err','Pomoćna funkcija nije dobro implementirana')
        struct('f',@test3, 'name','test3', 'err','Metoda sečice nije ispravna')
    };
end
    
function retVal = test1()
    retVal = zapremina();
end

function retVal = test2()    
    retVal = zadatak2(5);
end

function retVal = test3()
    H = 7;
    V = 20;
    retVal = secica(@(x) (x^2 * pi * H - 20), 0, 20, 100, 0.01);
end