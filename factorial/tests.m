function fs = tests()
    fs = { 
        struct('f',@test1, 'name','test1', 'err','my_factorial(10) nije ispravno')
        struct('f',@test2, 'name','test2', 'err','my_factorial(5) nije ispravno')
        struct('f',@test3, 'name','test3', 'err','my_factorial(0) nije ispravno')
    };
end
    
function retVal = test1()
    retVal = my_factorial(10);
end

function retVal = test2()
    retVal = my_factorial(5);
end

function retVal = test3()
    retVal = my_factorial(0);
end
