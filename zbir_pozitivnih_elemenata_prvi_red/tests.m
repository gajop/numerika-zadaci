function fs = tests()
    fs = { 
        struct('f',@test1, 'name','test1', 'err','Dobijeni rezultat nije ispravan')
        struct('f',@test2, 'name','test2', 'err','Rezultat nije dobar u slučaju postojanja negativnih elemenata')
    };
end
    
function retVal = test1()
    A = [ 1 2 3; 4 5 6; 7 8 9];
    retVal = zadatak4(A);
end

function retVal = test2()    
    A = [ -1 2 -3; 4 5 6; 7 8 9];
    retVal = zadatak4(A);
end
