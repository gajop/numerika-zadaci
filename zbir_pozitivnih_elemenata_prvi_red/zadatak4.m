function s = zadatak4(A)
[n m] = size(A);
s = 0;
for i = 1:m
  if A(1, i) > 0
    s = s + A(1, i);
  end
end