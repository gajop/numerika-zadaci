function fs = tests()
    fs = { 
        struct('f',@test1, 'name','test1', 'err','Konačni rezultat nije ispravan')
        struct('f',@test2, 'name','test2', 'err','Poziv Gaus Zajdela nije ispravan')
        struct('f',@test3, 'name','test3', 'err','Gaus Zajdel nije ispravan')
    };
end
    
function retVal = test1()
    retVal = zadatak1();
end

function retVal = test2()    
    retVal = postavka();
end

function retVal = test3()
    A = [2 3; 3 5];
    b = [5 12];
    retVal = gz(A, b, [1 1 1], 100, 0.01);
end