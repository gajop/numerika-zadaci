function x = gz(A, b, x0, itMax, errMax)
[n m] = size(A);
x = zeros(n,1);
for it = 1:itMax
    for i=1:n
	s = 0;
	for j=1:i-1
	    s = s + A(i,j)*x(j);
	end
	for j =i+1:n
	    s = s + A(i,j)*x0(j);
	end
	x(i) = (b(i)-s)/A(i,i);
    end
    err = 0;
    for i=1:n
	err = err+(x(i)-x0(i))^2;
    end
    if (err<errMax^2)
	return;
    end
    for i=1:n
	x0(i) = x(i);
    end
end